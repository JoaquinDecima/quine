# Quine

> En programación, un quine (pronunciado "kwain") es un programa (un tipo de Metaprogramación) que produce su código fuente como su salida única.

Con esto decimos que nuestro código es capaz de imprimirse como salida, de esta manera puede replicarse.

---

## Prerrequisitos

Como requisito previo requiere la instalación de python 3. Para poder usar el Start es necesario disponer de Bash, es decir que lo idea es estar en linux. Si estas en linux dejo la documentación necesaria para poder ejecutar el start.

### Debian/Ubuntu

```bash
sudo apt install python3 cat git
```

### Arch + yay

```bash
sudo yay -S python3 cat git
```

### Arch

```bash
sudo pacman -S python3 cat git
```

---

## Uso

Una vez que tengamos todo instalado podemos descargarlo y ejecutarlo para lo cual voy a dejarte el comando para poder ejecutarlo con simpleza

```bash
git clone https://gitlab.com/JoaquinDecima/quine.git
cd quine
./start
```

---

## El código

El código está escrito en python y es completamente visible e inofensivo, el mismo solo se imprime. Te lo dejo acá para que lo veas de antemano.

```python
print((lambda x:f"{x}{x,})")('print((lambda x:f"{x}{x,})")',))
```

---

## Información

* **Autor:** Joaquin (Pato) Decima
* **Año:** 2020
* **Instituto:** Universidad Nacional de Quilmes
* **Clase:** Seguridad Informática
